#include "input_manager.h"

InputManager::InputManager()
    : mouseCoordinate({ 0, 0 })
{
    exit = false;
}

InputManager::~InputManager()
{
    Dispose();
}

void InputManager::Update(SDL_Event event)
{
    for (auto& it : keyMap)
    {
        previousKeyMap[it.first] = it.second;
    }

    wheelX = wheelY = false;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                exit = true;
                break;
            case SDL_KEYDOWN:
                PressKey(event.key.keysym.sym);
                break;
            case SDL_KEYUP:
                ReleaseKey(event.key.keysym.sym);
                break;
            case SDL_MOUSEMOTION:
                SetMouseCoordinate(event.motion.x, event.motion.y);
                break;
            case SDL_MOUSEWHEEL:
                WheelEvent(event.wheel.x, event.wheel.y);
                break;
            case SDL_MOUSEBUTTONDOWN:
                PressKey(event.button.button);
                break;
            case SDL_MOUSEBUTTONUP:
                ReleaseKey(event.button.button);
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
                {
                }
                if (event.window.event == SDL_WINDOWEVENT_LEAVE)
                {
                }
                break;
        }
    }
}

void InputManager::WheelEvent(int, int) {}

void InputManager::SetMouseCoordinate(int x, int y)
{
    mouseCoordinate.x = x;
    mouseCoordinate.y = y;
}

void InputManager::PressKey(unsigned int keyID)
{
    keyMap[keyID] = true;
}

void InputManager::ReleaseKey(unsigned int keyID)
{
    keyMap[keyID] = false;
}

bool InputManager::IsKeyDown(unsigned int keyID)
{
    auto it = keyMap.find(keyID);
    return it != keyMap.end() ? it->second : false;
}

bool InputManager::IsKeyPressed(unsigned int keyID)
{
    if (IsKeyDown(keyID) == true && WasKeyDown(keyID) == false)
    {
        return true;
    }
    return false;
}

void InputManager::Dispose()
{
    keyMap.clear();
    previousKeyMap.clear();
}

bool InputManager::WasKeyDown(unsigned int keyID)
{
    auto it = previousKeyMap.find(keyID);
    return it != previousKeyMap.end() ? it->second : false;
}
