#include "texture_manager.h"

#include <iostream>

TextureManager::TextureManager(SDL_Renderer* inputRenderer)
{
    this->renderer = inputRenderer;
}

TextureManager::~TextureManager()
{
    Dispose();
}

void TextureManager::Dispose()
{
    for (auto i = textureMap.begin(); i != textureMap.end(); ++i)
    {
        SDL_DestroyTexture(i->second);
    }

    textureMap.clear();
}

SDL_Texture* TextureManager::GetTexture(std::string name)
{
    std::map<std::string, SDL_Texture*>::iterator mit = textureMap.find(name);

    if (mit == textureMap.end())
    {
        SDL_Texture* newTexture = loadTexture(name);
        textureMap.insert(make_pair(name, newTexture));
        return newTexture;
    }

    return mit->second;
}

SDL_Texture* TextureManager::loadTexture(std::string name)
{
    SDL_Surface* loadSurface = IMG_Load(name.c_str());

    if (!loadSurface)
    {
        std::cout << "Error: " << IMG_GetError << std::endl;
    }

    SDL_Texture* newTexture =
        SDL_CreateTextureFromSurface(renderer, loadSurface);
    SDL_FreeSurface(loadSurface);

    return newTexture;
}
