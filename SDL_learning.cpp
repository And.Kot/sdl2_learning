﻿#include "input_manager.h"
#include "sdl_sprite.h"
#include "texture_manager.h"

#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <windows.h>

const Uint32 width  = 800;
const Uint32 height = 600;

SDL_Window*   window;
SDL_Renderer* renderer;
SDL_Event     event;

int main(int args, char**)
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        std::cout << "SDL_Init: error: " << SDL_GetError() << std::endl;
    }

    if (IMG_Init(IMG_INIT_PNG) < 0)
    {
        std::cout << "IMG_Init: error: " << IMG_GetError << std::endl;
    }

    Uint32 flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL;

    window = SDL_CreateWindow("SDL2_learning", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, width, height, flags);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    TextureManager textureManager(renderer);

    SDLSprite sprite;
    sprite.SetTexture(textureManager.GetTexture("anim_fireball.png"));

    float angel = 0.0;

    bool loop = true;

    sprite.SetPosition(width / 2, height / 2);

    InputManager inputManager;

    while (loop)
    {
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);

        SDL_RenderClear(renderer);

        inputManager.Update(event);

        if (inputManager.exit)
        {
            loop = false;
        }

        if (inputManager.IsKeyDown(SDLK_LEFT))
        {
            sprite.MovePosition(-0.5f, 0.0f);
        }
        if (inputManager.IsKeyDown(SDLK_RIGHT))
        {
            sprite.MovePosition(0.5f, 0.0f);
        }
        if (inputManager.IsKeyDown(SDLK_DOWN))
        {
            sprite.MovePosition(0.0f, +0.5f);
        }
        if (inputManager.IsKeyDown(SDLK_UP))
        {
            sprite.MovePosition(0.0f, -0.5f);
        }

        angel += 0.05;
        float temp = angel;

        static int   nextPortion = 0;
        static float time        = 0;
        static int   reSender    = -1;

        sprite.SetColor(255, 255, 255, 255);
        sprite.SetScale(1, 1);
        sprite.SetRectangle(0, nextPortion, 511, 800 / 5);
        sprite.SetOrigin(511 / 2, 800 / 5 / 2);

        sprite.SetRotation(0);
        sprite.DrawSprite(renderer);

        SDL_RenderPresent(renderer);

        if (nextPortion >= 800)
        {
            nextPortion = 0;
        }

        time += 0.1;
        // reSender += reSender;
        if (time > 50)
        {
            nextPortion += 800 / 5;
            time = 0;

            sprite.MovePosition(2 * reSender, 5 * reSender);
            reSender *= (-1);
        }

        // Sleep(200);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
