#pragma once

#include <SDL.h>
#include <SDL_image.h>

class __declspec(dllexport) SDLSprite {
public:
  SDLSprite();
  ~SDLSprite();

  void SetPosition(float x, float y);
  void SetPosition(const SDL_FPoint &inputPosition);

  void SetScale(float x, float y);
  void SetScale(const SDL_FPoint &inputScale);

  void SetTexture(SDL_Texture *inputTexture);

  void SetRectangle(SDL_Rect inputRectangle);
  void SetRectangle(int x, int y, int w, int h);
  void SetRectangle(float x, float y, float w, float h);

  void SetOrigin(float x, float y);
  void SetOrigin(const SDL_FPoint &inputOrigin);

  void SetRotation(float inputAngel);

  void SetColor(const uint8_t r, const uint8_t g, const uint8_t b,
                const uint8_t a);
  void SetColor(const SDL_Color &inputColor);

  SDL_FPoint GetPosition() { return position; }
  SDL_FPoint GetScale() { return scale; }

  SDL_FPoint GetOrigin() { return origin; }
  SDL_Texture *GetTexture() { return texture; }
  SDL_FRect GetRectangle();
  SDL_Color GetColor() { return color; }

  void DrawSprite(SDL_Renderer *inputRenderer);

  void MovePosition(SDL_FPoint move);
  void MovePosition(float x, float y);

private:
  float angel;
  SDL_FPoint position;
  SDL_FPoint scale;
  SDL_FPoint origin;
  SDL_Texture *texture;
  SDL_Rect rectangle;

  SDL_Color color;

  void Init();
};
