#include "sdl_sprite.h"

#include <iostream>

SDLSprite::SDLSprite() {}

SDLSprite::~SDLSprite() {}

void SDLSprite::SetPosition(float x, float y) { position = {x, y}; }

void SDLSprite::SetPosition(const SDL_FPoint &inputPosition) {
  position = inputPosition;
}

void SDLSprite::SetScale(float x, float y) { scale = {x, y}; }

void SDLSprite::SetScale(const SDL_FPoint &inputScale) { scale = inputScale; }

void SDLSprite::SetTexture(SDL_Texture *inputTexture) {
  texture = inputTexture;

  SDL_QueryTexture(texture, NULL, NULL, &rectangle.w, &rectangle.h);

  Init();
}

void SDLSprite::SetRectangle(SDL_Rect inputRectangle) {
  rectangle = inputRectangle;
}

void SDLSprite::SetRectangle(int x, int y, int w, int h) {
  rectangle = {x, y, w, h};
}

void SDLSprite::SetRectangle(float x, float y, float w, float h) {
  rectangle = {(int)x, (int)y, (int)w, (int)h};
}

void SDLSprite::SetOrigin(float x, float y) { origin = {x, y}; }

void SDLSprite::SetOrigin(const SDL_FPoint &inputOrigin) {
  origin = inputOrigin;
}

void SDLSprite::SetRotation(float inputAngel) { angel = inputAngel; }

void SDLSprite::DrawSprite(SDL_Renderer *inputRenderer) {
  SDL_FPoint recountOrigin = origin;

  recountOrigin.x *= scale.x;
  recountOrigin.y *= scale.y;

  SDL_FRect rectWin{position.x - recountOrigin.x, position.y - recountOrigin.y,
                    rectangle.w * scale.x, rectangle.h * scale.y};

  SDL_SetTextureColorMod(texture, color.r, color.g, color.b);
  SDL_GetTextureAlphaMod(texture, &color.a);

  SDL_RenderCopyExF(inputRenderer, texture, &rectangle, &rectWin, angel,
                    &recountOrigin, SDL_FLIP_NONE);
}

void SDLSprite::SetColor(const uint8_t r, const uint8_t g, const uint8_t b,
                         const uint8_t a) {
  color = {r, g, b, a};
}

void SDLSprite::SetColor(const SDL_Color &inputColor) { color = inputColor; }

SDL_FRect SDLSprite::GetRectangle() {
  SDL_FPoint recountOrigin = origin;

  recountOrigin.x *= scale.x;
  recountOrigin.y *= scale.y;

  SDL_FRect rectWin{position.x - recountOrigin.x, position.y - recountOrigin.y,
                    rectangle.w * scale.x, rectangle.h * scale.y};

  return rectWin;
}

void SDLSprite::Init() {
  angel = 0.0;
  position = {0.0, 0.0};
  scale = {1.0, 1.0};
  origin = {0, 0};
  rectangle.x = rectangle.y = 0;

  color = {255, 255, 255, 255};
}

void SDLSprite::MovePosition(SDL_FPoint move) {
  position.x += move.x;
  position.y += move.y;
}

void SDLSprite::MovePosition(float x, float y) {
  position.x += x;
  position.y += y;
}
