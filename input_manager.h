#pragma once

#include <SDL.h>

#include <unordered_map>

class __declspec(dllexport) InputManager
{
public:
    bool exit;

    int wheelX;
    int wheelY;

    InputManager();
    ~InputManager();

    void Update(SDL_Event);

    void WheelEvent(int, int);
    void SetMouseCoordinate(int, int);

    void PressKey(unsigned int);
    void ReleaseKey(unsigned int);

    bool IsKeyDown(unsigned int);
    bool IsKeyPressed(unsigned int);

    void Dispose();

    // getters
    SDL_Point GetMouseCoordinate() { return mouseCoordinate; }

private:
    SDL_Point mouseCoordinate;

    std::unordered_map<unsigned int, bool> keyMap;
    std::unordered_map<unsigned int, bool> previousKeyMap;

    bool WasKeyDown(unsigned int);
};
