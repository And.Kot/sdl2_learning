#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include <cstdio>
#include <map>
#include <string>

class __declspec(dllexport) TextureManager
{
public:
    TextureManager(SDL_Renderer* inputRenderer);
    ~TextureManager();

    void         Dispose();
    SDL_Texture* GetTexture(std::string name);

private:
    SDL_Renderer* renderer;
    SDL_Texture*  loadTexture(std::string name);

    std::map<std::string, SDL_Texture*> textureMap;
};
